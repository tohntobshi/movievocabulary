import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducers from './reducers';
import sagas from './sagas';

const sagaMiddleWare = createSagaMiddleware();
const store = createStore(reducers, applyMiddleware(sagaMiddleWare));
sagaMiddleWare.run(sagas);

window.addEventListener('resize', () => {
    store.dispatch({
        type: 'SCREEN_RESIZE',
        payload: window.innerWidth < 760
    });
});

export { store as default };