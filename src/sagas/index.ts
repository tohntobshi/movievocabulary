import { call, put, takeEvery, all } from 'redux-saga/effects';
import { searchSubs, fetchWords, fetchTranslation } from '../apis';
import { history } from '../routes';
import { showTranslation } from '../actions';
import { Action, SearchQuery, SearchResult } from '../interfaces';

function* search(action: Action<SearchQuery>) {
  const response: SearchResult = yield call(searchSubs, action.payload);
  yield put({
    type: 'SEARCH_SUCCESS',
    payload: response
  });
}

function* translate(action: Action<string>) {
  const response = yield call(fetchTranslation, action.payload);
  yield put({
    type: 'WRITE_TRANSLATION',
    payload: {
      word: action.payload,
      defs: response.def
    }
  });
  yield put(showTranslation(action.payload));
}

function* requestWords(action: Action<string>) {
  const response: string[] = yield call(fetchWords, action.payload);
  yield put({
    type: 'WORDS_RECEIVED',
    payload: response
  });
}

function* onWordsReceived() {
  yield call(history.push as any, '/step2');
}

function* searchWatcher() {
  yield takeEvery('SEARCH_REQUEST', search);
}

function* fetchWordsWatcher() {
  yield takeEvery('FETCH_WORDS', requestWords);
}

function* wordsReceivedWatcher() {
  yield takeEvery('WORDS_RECEIVED', onWordsReceived);
}

function* fetchTranslationWatcher() {
  yield takeEvery('FETCH_TRANSLATION', translate);
}

export default function* () {
  yield all([
    searchWatcher(),
    wordsReceivedWatcher(),
    fetchTranslationWatcher(),
    fetchWordsWatcher()
  ]);
}
