import { SearchQuery, SearchResult } from '../interfaces';

const OS = require('opensubtitles-api');

export function searchSubs({ query, season, episode, imdbid }: SearchQuery): Promise<SearchResult> {
    return new Promise((resolve, reject) => {
      const OpenSubtitles = new OS({
        useragent: 'TemporaryUserAgent',
        ssl: true
      });
      OpenSubtitles.search({
        sublanguageid: 'eng',
        query,
        season,
        episode,
        imdbid
      }).then((result: any) => {
        if (result.en) {
            resolve({
                filename: result.en.filename,
                url: result.en.url
            });
        } else {
          reject();
        }
      }).catch((e: any) => {
        reject(e);
      });
    });
  }