const excludeWords: any = ['ll', 're', 'am', 'an', 'it', 'on', 'ain', 'aren'];

export function fetchWords(url: string): Promise<string[]> {
  return new Promise((resolve, reject) => {
    fetch(url).then(r => {
      r.text().then(t => {
        let allwords = t.toLowerCase().match(/[a-zA-Z]+\-[a-zA-Z]+|[a-zA-Z]{2,}/g);
        if (!allwords) {
          reject();
          return;
        }
        allwords = Array.from(new Set(allwords))
              .sort((a, b) =>  a > b ? 1 : -1)
              .filter(a => {
                if (excludeWords.includes(a)) {
                  return false;
                } else {
                  return true;
                }
              });
        resolve(allwords);
      }).catch(e => {
        reject(e);
      });
    }).catch(e => {
      reject(e);
    });
  });
}
