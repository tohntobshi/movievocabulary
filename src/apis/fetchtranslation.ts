import { Definition } from '../interfaces';

export function fetchTranslation(word: string): Promise<Definition[]> {
    const key = 'dict.1.1.20180315T135006Z.1a2e795450c39084.7190735652953414f06c2cf17fb5efe0dd9d186d';
    let url = 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=' + key + '&lang=en-ru&text=' + word;
    return fetch(url).then(r => r.json());
}