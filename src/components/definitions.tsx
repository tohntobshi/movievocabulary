import * as React from 'react';
import { connect } from 'react-redux';
import DefinitionComponent from './definition';
import { State, Word, Definition } from '../interfaces';
import './definitions.css';

interface Props {
    word: Word;
}

const Definitions = ({word: {defs}}: Props) => {
    if (defs) {
        return (
            <div className='DefinitionsComponent'>
                <ul>
                    {defs.map((value: Definition, index: number) => 
                        <li key={index}><DefinitionComponent def={value} /></li>
                    )}
                </ul>
            </div>
        );
    } else {
        return <div />;
    }
};

function mapStateToProps({ words, currentWord }: State) {
    return {
        word: currentWord !== null ? words[currentWord] : {}
    };
}

export default connect(mapStateToProps)(Definitions);