import * as React from 'react';
import { connect } from 'react-redux';
// import { List, ListItem } from 'material-ui/List';
import { translate } from '../actions';
import { Word, State } from '../interfaces';

interface Props {
  words: Word[];
  _translate: any;
  _currentWord: string;
  onClick?(): void;
}

const style = {
  listStyle: 'none',
  lineHeight: '2.2',
  cursor: 'pointer',
  paddingLeft: '0'
};

const SelectedWords = ({ words, _translate, _currentWord, onClick }: Props) => {
  const selected = { backgroundColor: 'grey', color: 'white' };
  const normal = { paddingLeft: '10px'};
  return (
    <ul style={style}>
      {words.map((word) => (
        <li
          key={word.word}
          style={word.word === _currentWord ? {...selected, ...normal} : normal}
          onClick={() => {
            _translate(word.word);
            if (onClick) {
              onClick();
            }
          }}
        >
          {word.word}
        </li>
      ))}
    </ul>
  );
};

function mapStateToProps({ words, currentWord }: State) {
  return {
    words: words.filter(word => word.selected),
    _currentWord: currentWord !== null ? words[currentWord].word : ''
  };
}
function mapDispatchToProps(dispatch: any) {
  return {
    _translate: function (word: string) {
      dispatch(translate(word));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectedWords);
