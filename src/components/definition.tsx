import * as React from 'react';
import { Definition } from '../interfaces';

interface Props {
    def: Definition;
}

const DefinitionComponent = ({def}: Props): any => {
    return (
        <div>
            <h4>{def.text || false}</h4>
            <p>{def.ts ? `[${def.ts}]` : false}</p>
            <ul>
                {def.tr && def.tr.map((val, index) => <li key={index}><DefinitionComponent def={val}/></li>)}
            </ul>
            {def.ex && <p className='info'>прим</p>}
            <ul>
                {def.ex && def.ex.map((val, index) => <li key={index}><DefinitionComponent def={val}/></li>)}
            </ul>
            {def.syn && <p className='info'>рус син</p>}
            <ul>
                {def.syn && def.syn.map((val, index) => <li key={index}><DefinitionComponent def={val}/></li>)}
            </ul>
            {def.mean && <p className='info'>анг син</p>}
            <ul>
                {def.mean && def.mean.map((val, index) => <li key={index}><DefinitionComponent def={val}/></li>)}
            </ul>
        </div>
    );
};

export { DefinitionComponent as default };