import * as React from 'react';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import { connect } from 'react-redux';
import { searchRequest } from '../actions';
import { SearchQuery } from '../interfaces';

const style = {
  textAlign: 'center'
};

interface Props {
  search: any;
}

class Search extends React.Component<Props> {
  state = {
    query: '',
    season: '',
    episode: '',
    imdbid: ''
  };
  onChange = (name: string) => (event: any) => {
    this.setState({
      [name]: event.target.value
    });
  }
  search = () => {
    if (!this.state.query && !this.state.imdbid) {
      return;
    }
    this.props.search(this.state);
  }
  render() {
    return (
      <div style={style}>
        <TextField 
          onChange={this.onChange('query')}
          hintText="Запрос"
        />
        <br />
        <TextField 
          onChange={this.onChange('season')}
          hintText="Сезон"
        />
        <br />
        <TextField 
          onChange={this.onChange('episode')}
          hintText="Серия"
        />
        <br />
        <TextField 
          onChange={this.onChange('imdbid')}
          hintText="IMDB"
        />
        <br />
        <FlatButton 
          label="Поиск" 
          primary={true} 
          onClick={this.search} 
        />
      </div>
    );
  }

}

function mapStateToProps (state: any) {
  return {};
}

function mapDispatchToProps (dispatch: any) {
  return {
    search: (searchQuery: SearchQuery) => {
      dispatch(searchRequest(searchQuery));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
