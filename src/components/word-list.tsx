import * as React from 'react';
import { connect } from 'react-redux';
// import { List, ListItem } from 'material-ui/List';
import { selectWord } from '../actions';
import { State, Word } from '../interfaces';

const style = {
  textAlign: 'center',
  listStyle: 'none',
  lineHeight: '2.2',
  padding: '0',
  cursor: 'pointer'
};

interface Props {
  words: Word[];
  select: any;
}

const WordList = ({ words, select }: Props) => {
  return (
    <ul style={style}>
      {words.map((word, index) => (
        <React.Fragment>
          {index % 10 === 0 && <hr style={{maxWidth: '400px'}}/>}
          <li
            key={word.word}
            style={word.selected ? { backgroundColor: 'grey', color: 'white' } : {}}
            onClick={select.bind(null, word.word)}
          >
            {word.word}
          </li>
        </ React.Fragment>
      ))}
    </ul>
  );
};

function mapStateToProps({ words }: State) {
  return {
    words
  };
}
function mapDispatchToProps(dispatch: any) {
  return {
    select: function (word: string) {
      dispatch(selectWord(word));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WordList);
