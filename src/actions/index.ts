import store from '../store';
import { Word, State, SearchQuery, Action } from '../interfaces';

export function translate(word: string) {
    let { words } = store.getState() as State;
    let index = words.findIndex((w: Word) => w.word === word);
    if (words[index].defs) {
        return showTranslation(word);
    } else {
        return fetchTranslation(word);
    }
}

export function searchRequest({ query, season, episode, imdbid }: SearchQuery): Action<SearchQuery> {
    return {
        type: 'SEARCH_REQUEST',
        payload: {
            query,
            season,
            episode,
            imdbid
        }
    };
}

export function selectWord(word: string): Action<string> {
    return {
        type: 'SELECT_WORD',
        payload: word
    };
}

export function showTranslation(word: string): Action<number> {
    let { words } = store.getState() as State;
    let index = words.findIndex((w: Word) => w.word === word);
    return {
        type: 'SHOW_TRANSLATION',
        payload: index
    };
}

export function fetchTranslation(word: string): Action<string> {
    return {
        type: 'FETCH_TRANSLATION',
        payload: word
    };
}

export function fetchWords(): Action<string> {
    let { filename: { url } } = store.getState() as State;
    return {
        type: 'FETCH_WORDS',
        payload: url
    };
}

export function translateFirst(remembered: boolean = false) {
    let { words } = store.getState() as State;
    let index = words.findIndex(word => (word.selected && (remembered || !word.remembered)) ? true : false );
    if (index >= 0) {
        return translate(words[index].word);
    } else {
        return finishExercise();
    }
}

export function translateNext(remembered: boolean = false) {
    let { words, currentWord } = store.getState() as State;
    let index = words.findIndex(
        (word, _index) => 
            (word.selected && (remembered || !word.remembered) && _index > (currentWord || 0)) ? true : false 
    );
    if (index >= 0) {
        return translate(words[index].word);
    } else {
        return translateFirst();
    }
    
}

export function finishExercise(): Action<undefined> {
    return {
        type: 'FINISH_EXERCISE',
        payload: undefined
    };
}

export function rememberWord(): Action<number> {
    let { currentWord } = store.getState() as State;
    return {
        type: 'WORD_REMEMBERED',
        payload: currentWord || 0
    };
}

export function repeatExercise(): Action<undefined> {
    return {
        type: 'REPEAT_EXERCISE',
        payload: undefined
    };
}