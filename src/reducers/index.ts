import { combineReducers } from 'redux';
import { Action, Word, SearchResult } from '../interfaces';

function searchingSubs(state: boolean = false, action: Action<any>): boolean {
  switch (action.type) {
    case 'SEARCH_REQUEST':
      return true;
    case 'SEARCH_SUCCESS':
      return false;
    default:
      return state;
  }
}

function fetchingWords(state: boolean = false, action: Action<any>): boolean {
  switch (action.type) {
    case 'FETCH_WORDS':
      return true;
    case 'WORDS_RECEIVED':
      return false;
    default:
      return state;
  }
}

function words(state: Word[] = [], action: Action<any>): Word[] {
  let index, newState;
  switch (action.type) {
    case 'WORDS_RECEIVED':
      return action.payload.map((word: string) => ({ word, selected: false }));
    case 'SELECT_WORD':
      index = state.findIndex(word => word.word === action.payload);
      newState = [...state];
      newState[index] = { ...state[index], selected: !state[index].selected };
      return newState;
    case 'WRITE_TRANSLATION':
      index = state.findIndex(word => word.word === action.payload.word);
      newState = [...state];
      newState[index] = { ...state[index], defs: action.payload.defs };
      return newState;
    case 'WORD_REMEMBERED':
      index = action.payload;
      newState = [...state];
      newState[index] = { ...state[index], remembered: true};
      return newState;
    case 'REPEAT_EXERCISE':
      newState = state.map(val => ({...val, remembered: false}));
      return newState;
    default:
      return state;
  }
}

function filename(state: SearchResult = { filename: '', url: '' }, action: Action<any>): SearchResult {
  switch (action.type) {
    case 'SEARCH_SUCCESS':
      return action.payload;
    default:
      return state;
  }
}

function currentWord(state: number | null = null, action: Action<any>): number | null {
  switch (action.type) {
    case 'SHOW_TRANSLATION':
      return action.payload;
    default:
      return state;
  }
}

function exerciseFinished(state: boolean = false, action: Action<any>): boolean {
  switch (action.type) {
    case 'FINISH_EXERCISE':
      return true;
    case 'REPEAT_EXERCISE':
      return false;
    default:
      return state;
  }
}

function mobileView(state: boolean = window.innerWidth < 760, action: Action<any>): boolean {
  switch (action.type) {
    case 'SCREEN_RESIZE':
      return action.payload;
    default:
      return state;
  }
}

export default combineReducers({
  searchingSubs,
  fetchingWords,
  words,
  filename,
  currentWord,
  exerciseFinished,
  mobileView
});
