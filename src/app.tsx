import * as React from 'react';
import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Router, Route, Redirect } from 'react-router-dom';
import { Step1, Step2, Step3, Step4, history } from './routes';
import store from './store';

export default () => {
  return (
    <Provider store={store}>
      <MuiThemeProvider>
        <Router history={history}>
          <React.Fragment>
            <Route path='/' exact={true} render={() => <Redirect to='/step1'/>}/>
            <Route path='/step1' component={Step1}/>
            <Route path='/step2' component={Step2}/>
            <Route path='/step3' component={Step3}/>
            <Route path='/step4' component={Step4}/>
          </React.Fragment>
        </Router>
      </MuiThemeProvider>
    </Provider>
  );
};
