import * as React from 'react';
import Search from '../components/search';
import { connect } from 'react-redux';
import { State } from '../interfaces';
import FlatButton from 'material-ui/FlatButton';
import { fetchWords } from '../actions';
import CircularProgress from 'material-ui/CircularProgress';

interface Props {
    filename: string;
    fetchingWords: boolean;
    searchingSubs: boolean;
    _fetchWords(): void;
}

const style = {
    maxWidth: '760px',
    margin: '0 auto',
    textAlign: 'center',
    marginBottom: '100px'
};

const Step1 = ({filename, _fetchWords, fetchingWords, searchingSubs}: Props) => {
    return (
        <div style={style}>
            <p>Данный сервис предназначен для тех, кто изучает английский по фильмам.
                Он позволяет ознакомиться с вокабуляром фильма перед просмотром,
                выбрать незнакомые слова и попытаться их запомнить с помощью простого упражнения.
            </p>
            <p>
                Информация берется из&#32;
                <a target='_blank' href='https://www.opensubtitles.org/'>OpenSubtitles</a>&#32;
                и&#32;
                <a target='_blank' href='https://tech.yandex.com/dictionary/'>Yandex dictionary</a> API
            </p>
            <p>
                (Сервис находится в разработке, возможны баги)
            </p>
            <p>Введите название фильма на английском языке, 
                для более точного поиска можно указать год после названия. 
                Для сериалов укажите сезон и серию в соответствующих полях.
                Так же можно искать по идентификатору IMDB
            </p>
            <img src='imdb.jpg' style={{maxWidth: '100%'}}/>
            <Search />
            {searchingSubs && <CircularProgress size={60} thickness={7} />}
            {
                filename && (
                    <div>
                        <p>В качестве источника слов будет использован следующий файл:</p>
                        <h4>{filename}</h4>
                        <p>Убедитесь что название файла похоже на то, что вы ищите, 
                            при необходимости повторите поиск, уточнив запрос.
                        </p>
                        <FlatButton label="Следующий шаг" primary={true} onClick={_fetchWords}/>
                    </div>
                )
            }
            {fetchingWords && <CircularProgress size={60} thickness={7} />}
        </div>
    );
};

function mapStateToProps({filename: { filename }, searchingSubs, fetchingWords }: State) {
    return {
        filename,
        searchingSubs,
        fetchingWords
    };
}

function mapDispatchToProps(dispatch: any) {
    return {
        _fetchWords: () => {
            dispatch(fetchWords());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Step1);