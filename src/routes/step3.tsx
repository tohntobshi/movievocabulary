import * as React from 'react';
import SelectedWords from '../components/selected-words';
import Definitions from '../components/definitions';
import FlatButton from 'material-ui/FlatButton';
import { connect } from 'react-redux';
import { State } from '../interfaces';
import { translateFirst, translateNext } from '../actions';
import { history } from './index';

const style = {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr'
};

const style2: any = {
    height: '100vh',
    overflow: "auto"
};

const style3: any = {
    height: '100vh',
    overflow: "auto"
};

interface Props {
    mobileView: boolean;
    _translateFirst(): void;
    _translateNext(): void; 
}

const Step3 = ({ _translateFirst, _translateNext, mobileView }: Props) => {
    const startExercise = () => {
        _translateFirst();
        history.push('/step4');

    };
    let divRef: any;
    const nextWord = () => {
        divRef.scrollTop = 0;
        _translateNext();
    };
    return (
        <div style={mobileView ? {} : style}>
            {!mobileView && (<div style={style2}>
                <SelectedWords
                    onClick={() => {
                        divRef.scrollTop = 0;
                    }}
                />
                <FlatButton
                    label="Перейти к проверке"
                    secondary={true}
                    onClick={startExercise}
                />
            </div>)}
            <div style={style3} ref={ref => divRef = ref}>
                <Definitions />
                {mobileView && (
                    <div>
                        <FlatButton
                            label="Следующее слово"
                            primary={true}
                            onClick={nextWord}
                        />
                        <br />
                        <FlatButton
                            label="Перейти к проверке"
                            secondary={true}
                            onClick={startExercise}
                        />
                    </div>
                )}
            </div>
        </div>
    );
};

function mapStateToProps({ mobileView }: State) {
    return {
        mobileView
    };
}

function mapDispatchToProps(dispatch: any) {
    return {
        _translateFirst: () => {
            dispatch(translateFirst());
        },
        _translateNext: () => {
            dispatch(translateNext(true));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Step3);