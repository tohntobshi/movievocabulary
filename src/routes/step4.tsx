import * as React from 'react';
import { connect } from 'react-redux';
import { State } from '../interfaces';
import Definitions from '../components/definitions';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import { translateNext, rememberWord, repeatExercise } from '../actions';

interface Props {
    word: string;
    finished: boolean;
    _rememberWord(): void;
    _dontRememberWord(): void;
    _repeatExercise(): void;
}

const style = {
    maxWidth: '760px',
    margin: '0 auto 10px auto',
    padding: '10px'
};

class Step4 extends React.Component<Props> {
    state = {
        checked: false,
        word: this.props.word,
        userTranslation: '',
        finished: this.props.finished
    };
    divRef: any;
    onUserTranslationChange = (event: any, newValue: string) => {
        this.setState({
            userTranslation: newValue
        });
    }
    checkTranslation = () => {
        this.setState({
            checked: true
        });
    }
    remember = () => {
        this.setState({
            userTranslation: '',
            checked: false
        });
        this.props._rememberWord();
        this.divRef.scrollTop = 0;
    }
    dontRemember = () => {
        this.setState({
            userTranslation: '',
            checked: false
        });
        this.props._dontRememberWord();
        this.divRef.scrollTop = 0;
    }
    componentWillReceiveProps({word, finished}: Props) {
        this.setState({
            word,
            finished
        });
    }
    render() {
        return !this.state.finished
            ? (
            <div style={style} ref={ref => this.divRef = ref}>
                <h2>{this.state.word}</h2>
                <TextField
                    hintText="Перевод"
                    onChange={this.onUserTranslationChange}
                    value={this.state.userTranslation}
                />
                <FlatButton 
                    label="Проверить"
                    onClick={this.checkTranslation}
                />
                {this.state.checked && 
                <div>
                    <Definitions />
                    <RaisedButton 
                        label="Помню" 
                        primary={true}
                        onClick={this.remember}
                    />&#32;
                    <RaisedButton 
                        label="Не помню" 
                        secondary={true} 
                        onClick={this.dontRemember}
                    />
                </div>
                }
            </div>
            ) : (
            <div style={style}>
                <h4>Упражнение завершено!</h4>
                <RaisedButton
                    label="Повторить"
                    onClick={this.props._repeatExercise}
                />
            </div>
            );
    }
}

function mapStateToProps({words, currentWord, exerciseFinished}: State) {
    return {
        word: currentWord !== null ? words[currentWord].word : '',
        finished: exerciseFinished
    };
}

function mapDispatchToProps(dispatch: any) {
    return {
        _rememberWord: () => {
            dispatch(rememberWord());
            dispatch(translateNext());
        },
        _dontRememberWord: () => {
            dispatch(translateNext());
        },
        _repeatExercise: () => {
            dispatch(repeatExercise());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Step4);
