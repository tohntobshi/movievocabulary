import * as React from 'react';
import WordList from '../components/word-list';
import RaisedButton from 'material-ui/RaisedButton';
import { connect } from 'react-redux';
import { State } from '../interfaces';
import { history } from './index';
import { translateFirst } from '../actions';

interface Props {
    _translateFirst(): void;
}

const Step2 = ({_translateFirst}: Props) => {
    const style = {
        textAlign: 'center',
        marginBottom: '10px'
    };
    const nextStep = () => {
        history.push('/step3');
        _translateFirst();
    };
    return (
        <div style={style}>
            <p>Выберите слова, которые вам не знакомы</p>
            <WordList />
            <RaisedButton label="Следующий шаг" primary={true} onClick={nextStep}/>
        </div>
    );
};

function mapStateToProps(state: State) {
    return {};
}

function mapDispatchToProps(dispatch: any) {
    return {
        _translateFirst: () => {
            dispatch(translateFirst(true));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Step2);