export interface Word {
    word: string;
    defs?: Definition[];
    selected?: boolean;
    remembered?: boolean;
}

export interface State {
    words: Word[];
    filename: SearchResult;
    currentWord: number|null;
    searchingSubs: boolean;
    fetchingWords: boolean;
    exerciseFinished: boolean;
    mobileView: boolean;
}

export interface Action<X> {
    type: string;
    payload: X;
}

export interface SearchQuery {
    query?: string;
    season?: string;
    episode?: string;
    imdbid?: string;
}

export interface Definition {
    text: string;
    pos?: string;
    ts?: string;
    gen?: string;
    tr?: Definition[];
    ex?: Definition[];
    syn?: Definition[];
    mean?: Definition[];
}

export interface SearchResult {
    filename: string;
    url: string;
}